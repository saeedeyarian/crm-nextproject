import mongoose from "mongoose";

async function connectDB() {
    try{
        if(mongoose.connections[0].readyState) return;
        mongoose.set("strictQuery",false)
        await mongoose.connect("mongodb://yariansaeedeh:4419567475@ac-7mlwrp9-shard-00-00.whgz3xx.mongodb.net:27017,ac-7mlwrp9-shard-00-01.whgz3xx.mongodb.net:27017,ac-7mlwrp9-shard-00-02.whgz3xx.mongodb.net:27017/?ssl=true&replicaSet=atlas-vhcdn1-shard-0&authSource=admin&retryWrites=true&w=majority")
        console.log("connected to DB")
    }catch(err) {
        console.log("Error in connecting to Data Base")
    }
}

export default connectDB;