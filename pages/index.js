import connectDB from "../utils/connectDB"
import Customer from "../models/Customer"
import Card from "../components/modules/Card"

export default function Home({customer}) {
  return (
    <div>
       {customer.map(customer => (
        <Card key={customer._id} customer={customer}/>
       ))}
    </div>
  )
}

export async function getServerSideProps() {

  try{
    await connectDB()
    const customer = await Customer.find()
    return {
      props : {
        customer : JSON.parse(JSON.stringify(customer))
      }
    }
  }catch(err) {
    return {notFound : true}
  }
}
