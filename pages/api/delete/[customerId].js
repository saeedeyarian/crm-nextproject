import connectDB from "../../../utils/connectDB"
import Customer from "../../../models/Customer"

export default async function handler(req,res) {

    try{
        await connectDB()
    } catch(err) {
        console.log(err)
        res.status(500).json({
            status:"faild",
            message : "error in conection to DB"
        })
        return
    }

    if(req.method === "DELETE") {
        const id = req.query.customerId

        try{
            await Customer.deleteOne({ _id : id})
            res.status(200).json({
                status:"success",
            })
        }catch(err) {
            console.log(err)
            res.status(500).json({
                status: "faild" ,
                message : "Error iin Deleting data"
            })
        }

    }
}