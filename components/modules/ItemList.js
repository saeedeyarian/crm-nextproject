import React from 'react'
import FormInput from './FormInput'

function ItemList({form,setForm}) {

    const {products} = form

    const AddHandler = () => {
      setForm({
        ...form,
        products : [...products , {name:"" ,price:"" , qty:""}]
      })
    }

    const changeHandler= (e,index) => {
        const {name,value} = e.target
        const newProducts = [...products]
        newProducts[index][name] = value
        setForm({
            ...form,
            products: newProducts
        })
    }

    const deleteHandler= (index) => {
      const newProducts = [...products]
      newProducts.splice(index,1)
      setForm({
        ...form,
        products: newProducts
      })
    }

  return (
    <div className="item-list">
      <h4>Purchased products</h4>
      {products.map( (product,index) => (
            <div key={index} className="form-input__list">
            <FormInput type="text" name="name" label="Product Name" value={product.name} onChange={e => changeHandler(e,index)} />
            <div>
                <FormInput type="text" name="price" label="Price" value={product.price} onChange={e => changeHandler(e,index)} />
                <FormInput type="text" name="qty" label="Qty" value={product.qty} onChange={e => changeHandler(e,index)} />
            </div>
            <button onClick={() => deleteHandler(index)}>Remove</button>
            </div>
        ))}
      <button onClick={AddHandler}>Add Item</button>
    </div>
  )
}

export default ItemList
