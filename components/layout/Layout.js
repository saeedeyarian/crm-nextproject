import Link from "next/link"

function Layout({children}) {
  return (
    <div>
      <header className="header">
        <h4>CRM Project</h4>
        <Link href="/Add-customer">Add Customer</Link>
      </header>
      <div className="main">
        {children}
      </div>
      <footer className="footer">
        CRM Projects | next courses
      </footer>
    </div>
  )
}

export default Layout
