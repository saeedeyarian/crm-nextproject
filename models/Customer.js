import { Schema,model,models } from "mongoose";

const customerSchemsa = new Schema({
    name : {
        type : String ,
        minLength : 1,
        required : true 
    },
    lastName : {
        type : String ,
        minLength : 1,
        required : true 
    },
    email : {
        type : String ,
        minLength : 1,
        required : true 
    },
    postalCode : Number ,
    phone : String ,
    date : Date ,
    address: String ,
    products : {
        type : Array ,
        default : []
    },
    createdAt : {
        type : Date ,
        default : () => Date.now()
    },
    updatedAt : {
        type : Date ,
        default : () => Date.now() 
    }
})

const Customer = models.Customer || model("Customer" , customerSchemsa)

export default Customer